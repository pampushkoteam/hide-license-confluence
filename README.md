## Project overview

Плагин скрывает из веб-интерфейса Confluence элементы сообщающие о статусе лицензии.
  
При этом для определенной группы интерфейс остаётся без изменений.
  
## Contributors

Alexander Pampushko
   
## License
  
Apache License 2.0