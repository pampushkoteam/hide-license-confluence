package pmpshk.confl.ui;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;

/**
 *
 */
public class HideLicenceCondition extends BaseConfluenceCondition
{
	
	@Override
	protected boolean shouldDisplay(WebInterfaceContext webInterfaceContext)
	{
		//todo : параметры метода почему-то равны null - разобраться почему
		
		boolean hideLicence = true; //по умолчанию скрываем лицензию для всех
		GroupManager groupManager = (GroupManager) ContainerManager.getComponent("groupManager");
		Group targetGroup = getTargetGroup(groupManager, "AWG-LM");
		
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		if (user != null)
		{
			boolean isTargetUser = false;
			try
			{
				isTargetUser = groupManager.hasMembership(targetGroup, user);
			}
			catch (EntityException e)
			{
				e.printStackTrace();
			}
			//...isUserInGroup(user, "AWG-LM");
			//если это юзер из указанной группы
			if (isTargetUser)
			{
				hideLicence = false; //то лицензию не скрываем
			}
		}
		
		return hideLicence;
	}
	
	private Group getTargetGroup(GroupManager groupManager, String groupName)
	{
		Group targetGroup = null;
		try
		{
			if (groupManager != null)
			{
				targetGroup = groupManager.getGroup(groupName);
			}
		}
		catch (EntityException e)
		{
			e.printStackTrace();
		}
		return targetGroup;
	}
	
}
