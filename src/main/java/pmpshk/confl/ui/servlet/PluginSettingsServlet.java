package pmpshk.confl.ui.servlet;

import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

public class PluginSettingsServlet extends HttpServlet
{
	private static final Logger log = LoggerFactory.getLogger(PluginSettingsServlet.class);
	private static final String CONTENT_TYPE = "text/html;charset=utf-8";
	private static final String VIEW = "configure.vm";
	
	@ComponentImport
	private transient UserManager userManager;
	@ComponentImport
	private transient LoginUriProvider loginUriProvider;
	@ComponentImport
	private transient final TemplateRenderer renderer;
	@ComponentImport
	private transient final PluginSettingsFactory pluginSettingsFactory;
	@ComponentImport
	private transient final TransactionTemplate transactionTemplate;
	
	
	@Inject
	public PluginSettingsServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer
			templateRenderer, PluginSettingsFactory pluginSettingsFactory, TransactionTemplate transactionTemplate)
	{
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.renderer = templateRenderer;
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.transactionTemplate = transactionTemplate;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		UserProfile userProfile = userManager.getRemoteUser(req);
		if (userProfile != null)
		{
			//если ты не сисадмин, то перенаправление на главную страницу
			if (!userManager.isSystemAdmin(userProfile.getUserKey()))
			{
				redirectToMain(req, resp);
				return;
			}
			else //если сисамдин, то показываем страницу сервлета
			{
				resp.setContentType("text/html;charset=utf-8");
				resp.getWriter().write("<html><body>Hello, I am plugin settings page!</body></html>");
			}
		}
		else //если ты не вошел в систему, то перенаправление на страницу логина
		{
			redirectToLogin(req, resp);
			return;
		}
	}
	
	private void redirectToMain(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		resp.sendRedirect(loginUriProvider.getLoginUri(URI.create("")).toASCIIString());
	}
	
	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	}
	
	private URI getUri(HttpServletRequest request)
	{
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null)
		{
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}
}