package pmpshk.confl.ui.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import pmpshk.confl.ui.api.MyPluginComponent;

import javax.inject.Named;

@ExportAsService({MyPluginComponent.class})
@Named("myPluginComponent")
public class MyPluginComponentImpl implements MyPluginComponent
{
	
	public MyPluginComponentImpl()
	{
	
	}
	
	public String getName()
	{
		return "myComponent";
	}
}